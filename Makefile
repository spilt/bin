PREFIX=

all:
	@echo "Nothing to do."

test:
	@./tests.sh

install:
	@prefix="$(PREFIX)"; \
	if [ ! "$$prefix" ]; then \
		printf '\033[1mWhere do you want to install? (default: /usr/local) \033[0m'; \
		read prefix; \
	fi; \
	[ ! "$$prefix" ] && prefix="/usr/local"; \
	mkdir -pv -m 755 "$$prefix/share/man/man1" "$$prefix/bin" \
	&& cp -v bin.1 "$$prefix/share/man/man1/" \
	&& rm -f "$$prefix/bin/bin" \
	&& cp -v bin "$$prefix/bin/"

uninstall:
	@prefix="$(PREFIX)"; \
	if [ ! "$$prefix" ]; then \
		printf '\033[1mWhere do you want to uninstall from? (default: /usr/local) \033[0m'; \
		read prefix; \
	fi; \
	[ ! "$$prefix" ] && prefix="/usr/local"; \
	echo "Deleting..."; \
	rm -rvf "$$prefix/bin/bin" "$$prefix/share/man/man1/bin.1"

.PHONY: all, test, install, uninstall

# bin - Console trash bin management

`bin` is a POSIX shell implementation of the Freedesktop.org trashcan
specification. It lets you run `bin <file>` to move a file to the trash bin
instead of just `rm <file>`, which permanently deletes it immediately. Later,
the file can be recovered with `bin -u <file>` or permanently erased with
`bin -e <file>`. More details are in the Usage section. This is based off of
[bashtrash](https://github.com/robrwo/bashtrash), but with a few more features
and a little bit cleaned up and tweaked to be fully POSIX-compliant, rather
than relying on bashisms.

## Requirements

`bin` requires the following utilities in addition to the standard tools
(`mv`, `rm`, etc.): `awk`, `basename`, `date`, `df`, `du`, `dirname`, `find`,
`mktemp`, `perl` (for URL encoding filenames), `readlink`, `sed`, `stty`,
`tput`.

Optionally, if you install my other tools `ask` and `arg`, you'll get slightly
nicer behavior, although `bin` should work perfectly fine without them.
(ask)[https://bitbucket.org/spilt/ask] will get you slightly cleaner
confirmation prompts, and (arg)[https://bitbucket.org/spilt/arg] will get more
robust command line flag parsing (without `arg`, you must use `bin -f -v -E`
instead of `bin -fvE`).

## Usage

Basic usage: `bin [OPTIONS]... FILES...`

Options:

* `-h`, `--help`           Show the program usage and exit
* `-V`, `--version`        Show program's version number and exit
* `-v`, `--verbose`        Explain what is being done
* `-i`, `--interactive`    Prompt before moving every file
* `-f`, `--force`          Ignore non-existent files, never prompt
* `-r`, `-R`, `--recursive`  Ignored (for compatability with `rm`)
* `-u`, `--untrash`        Restore file(s) from the trash
* `-e`, `--empty`          Choose files to empty from the trash
* `-E`, `--empty-all`      Empty all the files in the trash folder(s) (default: ~)
* `-l`, `--list`           List files in trash folder(s) (default: ~)
* `--`                     Any arguments after `--` will be treated as filenames

See `man bin` for full documentation.

## License

Copyright (c) 2020, Bruce Hill <bruce@bruce-hill.com>

Copyright (c) 2009-2011, Robert Rothenberg <robrwo@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

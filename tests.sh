#!/bin/sh
# Simple test suite
set -e
tmp="tests"
mkdir -p "$tmp"
trap 'rm -rf "$tmp"' EXIT

testdirs="d1 d2 d3 d4"
testfiles="f1 f2 f3"
for d in $testdirs; do
  mkdir -p "$tmp/$d"
  for f in $testfiles; do touch "$tmp/$d/$f"; done
done

./trash -v "$tmp/d1/f1" "$tmp/d1/f2" "$tmp/d2" "$tmp/d3"
./trash -v -f -u "$tmp/d1/f1"
./trash -v -f -u "$tmp/d2"

[ -e "$tmp/d1/f1" ]
! [ -e "$tmp/d1/f2" ]
[ -e "$tmp/d1/f3" ]
[ -e "$tmp/d2" ]
[ -e "$tmp/d2/f1" ]
! [ -e "$tmp/d3" ]
[ -e "$tmp/d4" ]

./trash -f -v -e "$tmp/d1/f2" "$tmp/d3"

echo "old" > "$tmp/over.txt"
./trash "$tmp/over.txt"
printf '\033[2mPausing 2sec to let file deletion time advance...\033[0m\n'
sleep 2
echo "new" > "$tmp/over.txt"
./trash -v "$tmp/over.txt"
./trash -v -u "$tmp/over.txt"
! [ -e "$tmp/over.txt" ]
[ -e "$tmp/over.txt" -a "$(cat "$tmp/over.txt")" = "new" ]
rm -f "$tmp/over.txt"
./trash -v -u "$tmp/over.txt"
[ -e "$tmp/over.txt" -a "$(cat "$tmp/over.txt")" = "old" ]

printf '\033[32;1mTests passed!\033[0m\n'
